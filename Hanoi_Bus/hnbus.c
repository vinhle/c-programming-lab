#include <gtk/gtk.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"jrb.c"
#include"jval.c"
#include"dllist.c"
#define MAX 100
#define TRUE 1
#define FALSE 0


typedef JRB Graph;

typedef struct {
  int visited;
  char before[30];
  JRB tree;
}Vertex;

typedef Vertex* vertex;


void on_window_destroy (GtkWidget *widget, gpointer data)
{
  gtk_main_quit ();
}


typedef Dllist tuyenxe;



Graph graph;
GtkWidget    *entry1, *entry2, *entry3, *entry4;
GtkWidget    *textview1;
GtkWidget *buffer;
Dllist all;
tuyenxe di[61], ve[61];
//=========ham chen mot node vao hang doi sau do sap xep luon theo kieu tang dan===========//
queue_insert(Dllist d, int n)
{
     int  flag;
     Dllist ptr;

     flag = 0; //flag = 0 la chua chen, bang 1 la da chen
     dll_traverse(ptr,d)
     {
       if(jval_i(ptr->val) > n)
            {
               if (flag ==0) dll_insert_b(ptr, new_jval_i(n));
               flag=1;
            }
       }
       if(flag==0) dll_append(d, new_jval_i(n));
}


so2chu(int so, char *chu)
{
  FILE *f;

  f = fopen("tam", "wt");
  fprintf(f, "%d", so);
  fclose(f);
  f = fopen("tam", "rt");
  fscanf(f, "%s", chu);
}

//huong dan ten ben ===========================================
int benchuatu(char *tu, char *ben)
{
   int i;
   char *ptr;
 
   ptr = ben;
   for(i = 0;i < strlen(ben);i++ ) 
   {
        if(ben[i] == ' ')
        {
           ben[i] = '\0';
           if(strcmp(tu,ptr)==0)
           {
              ben[i] = ' ';
              return 1;
           }
           ben[i] = ' ';
           ptr = &ben[i+1];
         } 
   }
   return 0;
}

taphopben(Dllist all)
{
   JRB node;
   jrb_traverse(node, graph)
        dll_append(all, new_jval_s(strdup(jval_s(node->key))));
}

huongdan(char *tenben)
{
   int i, flag;
   char *ptr;
   Dllist node;
   char ketqua[1000];

   ptr = tenben;
   printf("ten ben %s\n", ptr);
   strcpy(ketqua, "Suggest places:\n");
   for(i=0;i<=strlen(tenben);i++)
   {
         if(tenben[i] ==' '|| tenben[i] =='\0')
         {  
            if(tenben[i]==' ') flag = 1;
            else flag = 0; 
            tenben[i] = '\0';
            printf("ptr:  %s", ptr);
            dll_traverse(node,all )
            {
                 if(benchuatu(ptr, jval_s(node->val)))
                 {
                      strcat(ketqua, jval_s(node->val));     //cho nay
                      strcat(ketqua, "\n");
                 }
           } 
           if(flag == 1) tenben[i] = ' ';
           else tenben[i] = '\0';
           ptr = &tenben[i+1];
         }
   }
   gtk_text_buffer_set_text(buffer, ketqua, -1);
}     
//ham them mot ben vao do thi
addEdge(Graph g, char * id1, char * id2, int w)
{
   JRB treenew, find1=NULL, find2= NULL;
   vertex vt;
   vertex vt2;
   char *vs1, *vs2;
   Dllist d;
   vs1 = (char*)malloc(sizeof(char)*30);
   vs2 = (char*)malloc(sizeof(char)*30);
   strcpy(vs1, id1);
   strcpy(vs2, id2);

   vt = (vertex)malloc(sizeof(Vertex));
   find1 = jrb_find_str(g, id1);
    
   if(find1!=NULL)
    {
           vt = (vertex)(jval_v(find1->val));
           find2 = jrb_find_str(vt->tree, id2);
           if(find2==NULL)
           {
	     d = new_dllist();
	     queue_insert(d, w);
	     jrb_insert_str(vt->tree, vs2, new_jval_v(d));
           }
           else 
	     {
	       d = (Dllist)jval_v(find2->val);
	       queue_insert(d, w);
	     }
               
    }
    else
    {
      d = new_dllist();
      queue_insert(d, w);
      vt2 = (vertex)malloc(sizeof(Vertex));
      vt2->tree = make_jrb();
      jrb_insert_str(vt2->tree, vs2, new_jval_v(d));
      jrb_insert_str(g, vs1, new_jval_v(vt2));
    }

}

Read_file(FILE *fin, Graph g) //Nhap du lieu tu file
{
    int busline;
    char source[50], name[50], tmp[50];
    int i = 0, test, flag = 0, j;

    while(1)
    {
      fscanf(fin, "%d", &busline); //Lay ra so tuyen
      if(feof(fin)) return; //Phai sau khi lay khong duoc mot ki tu moi ket thuc
      if(flag==0) di[busline] = new_dllist();  // = 0 la di , = 1 la ve
      else ve[busline] = new_dllist();

      fscanf(fin, "%s", source); //Khoi tao ben nguon
      //printf("%d %s ", busline, source);
      strcpy(tmp, source);
      for(j=0;j<strlen(tmp);j++) if(tmp[j]=='-') tmp[j] = ' ';
      if(flag==0) dll_append(di[busline], new_jval_s(strdup(tmp)));
      else dll_append(ve[busline], new_jval_s(strdup(tmp)));
      
      do
      {
        fscanf(fin, "%s", name);
        if(feof(fin)) return; //Thuc ra cho nay khon can dat cho co kha nang chiu loi
        for(j=0;j<strlen(name); j++) if(name[j] == '-') name[j] = ' ';
        if(flag==0) dll_append(di[busline], new_jval_s(strdup(name)));
        else dll_append(ve[busline], new_jval_s(strdup(name)));
        addEdge(g, tmp, name, busline);
        strcpy(tmp, name);
        
      }while(fgetc(fin) != '\n');
      if(flag==0) flag = 1;
        else flag = 0;
    }
   return;
}


Graph createGraph()
{
 Graph g;
 FILE *f;
 Dllist node;

 g = make_jrb();
 f = fopen("bus.txt", "rt");
 if(f==NULL)
   {
     printf("loi khong co du lieu vao\n");
     return;
   }
 Read_file(f, g);
 all = new_dllist();
 printf("flag1\n");
 graph = g;
 taphopben(all);
 printf("tap hop cac ben\n");
 dll_traverse(node, all) printf("%s\n", jval_s(node->val));
 return g;
}
inlotrinhtuyen(int tuyen, char *ketqua)
{
   Dllist d;

   strcat(ketqua, "+ The route bus goes:\n");
   printf("lo trinh di\n");
   dll_traverse(d, di[tuyen])
   {
        printf("%s ", jval_s(d->val));
        strcat(ketqua, jval_s(d->val));
        if(d!=dll_last(di[tuyen])) strcat(ketqua, "--> ");
   }
   strcat(ketqua, "\n\n+ The route bus come back:\n");
   printf("lo trinh ve \n");
   dll_traverse(d, ve[tuyen])
   {
        printf("%s ", jval_s(d->val));
        strcat(ketqua, jval_s(d->val));
        if(d!=dll_last(ve[tuyen])) strcat(ketqua, "--> ");
  }
}

int BFS(Graph g, char *start, char *stop)  /*stop = -1 nghia la duyet het*/
{
  Dllist d, p; /*d la ten hang doi, p la con tro toi 1 node cua hang doi dang
                      xu ly*/
  JRB ptr, node, find;
  char v[30], ketqua[100];
  vertex vt,vt0;

  jrb_traverse(node, g)
   {
     vt0 = (vertex)jval_v(node->val);
     vt0->visited = FALSE;
     strcpy(vt0->before, "");
   }
/*kiem tra start va stop*/
  ptr = jrb_find_str(g, start);
  if(ptr==NULL) 
   {
     printf("do thi khong co ben %s\n", start);
     strcpy(ketqua, " Not have ");
     strcat(ketqua, start);
     strcat(ketqua, " in the map.\n");
     gtk_text_buffer_set_text(buffer, ketqua, -1);
     huongdan(start);
     return 0;
   }
  ptr =  jrb_find_str(g,stop);

  if(ptr==NULL && strcmp(stop,"all")!=0) 
   {
     printf("do thi khong co ben %s\n", stop);
     strcpy(ketqua, " Not have ");
     strcat(ketqua, stop);
     strcat(ketqua, " in the map.\n");
     gtk_text_buffer_set_text(buffer, ketqua, -1);
     huongdan(stop);
     return 0;
   }

  d = new_dllist();
  dll_append(d, new_jval_s(start));
  while(!dll_empty(d))
   {
     p = dll_first(d);       /*lay node dau ra*/
     strcpy(v,jval_s(p->val));      /*lay gia tri cua node p*/
     dll_delete_node(p);    /*xoa node p khoi hang doi*/
     ptr = jrb_find_str(g, v);
     if(ptr!=NULL)
       {
	 vt = (vertex)(jval_v(ptr->val));
         if(vt->visited==FALSE)
	   {
	     vt->visited = TRUE;
	     if(strcmp(v,stop)==0) return 1;
             jrb_traverse(node, vt->tree)
	       {    
		 dll_append(d, node->key);
		 find = jrb_find_str(g, jval_s(node->key));
	         if(find!=NULL)
		   {
		     vt0 =(vertex)jval_v(find->val);
		     if(vt0->visited == FALSE  && strcmp("",vt0->before)==0 ) strcpy(vt0->before,v);
		   } 
	       }
	   }
       }
   }
  return 1;
}


print(Graph g)
{
  JRB node, node1;
  vertex v;

  jrb_traverse(node, g)
    {
 //     printf("ben %s: ", jval_s(node->key));
      v =(vertex)jval_v( node->val);
      jrb_traverse(node1, v->tree)
	{
//	  printf("%s ", jval_s(node1->key));
	}
      printf("\n");
    }
} 


//cach ham phuc vu cho chuc nang tim duong di goi y======================
int mangzero(int *A)
{
  int i;
  for(i=0;i<=60;i++)
    if(A[i]!=0) return 0;
  return 1;
}

tronmang(int *A, int *B, int *C)
{
  int i;

  for(i=0;i<=60;i++)
    {
      if(A[i]==0 ||B[i]==0) C[i] = 0;
      else C[i] = 1;
    }
}

copymang(int *A, int *B)
{
  int i;
  for(i=0;i<=60;i++)
    A[i] = B[i];
}

taomangzero(int *A)
{
  int i;
  for(i=0;i<=60;i++) A[i] = 0;
}

int timnhonhat(int *A)
{
  int i;
  for(i=0;i<=60;i++)
    if(A[i] != 0) return i;
}
inmang(int *A)
{
  int i;
  for(i=0;i<=60;i++)
    if(A[i]!= 0) printf("%d\n", i);
}


//ham tim duong di==========================================
void path(Graph g, char * vs1, char * vs2)
{
  JRB find, find2;
  vertex v;
  int flag;
  Dllist d, p, d2, q, node, k;
  char ben[30], *phu, ketqua[3000], chu[4];
  int A[61], i, B[61], C[61];

//thuc hien chuc nang 1
  strcpy(ketqua,"+ The shortest route: \n");
  printf("lo trinh it ben nhat\n");
  d = new_dllist();
  flag = BFS(g, vs1, /*"all"*/vs2);
  if(flag==0) return;
  for(;;)
   {
     phu = (char*)malloc(sizeof(char)*30);
     strcpy(phu, vs2);

     dll_prepend(d, new_jval_s(phu));

     if(strcmp(vs2,vs1)==0) break;
     
     find = jrb_find_str(g, vs2);
     v = (vertex)jval_v(find->val);
     strcpy(vs2,v->before);
   }
  p = dll_first(d);
  while(1)
   {
     strcpy(ben,jval_s(p->val));     
     printf("%s ", ben);
     strcat(ketqua, ben);
     if(p!=dll_last(d)) strcat(ketqua, "-->");
     if(p==dll_last(d))break;
     p = dll_next(p);
   }  
 //thuc hien chuc nang 2
 printf("\ncu the\n");
 strcat(ketqua, "\n\nDetail:\n");
 p = dll_first(d);
 for(;;)
 {
   if(p==dll_last(d)) break ;
   q = dll_next(p);
   printf("+ %s --> %s : go by Bus.No: ", jval_s(p->val), jval_s(q->val));
   strcat(ketqua, "+ ");
   strcat(ketqua, jval_s(p->val));
   strcat(ketqua, " --> ");
   strcat(ketqua, jval_s(q->val));
   strcat(ketqua, " : go by Bus.No: ");

   find = jrb_find_str(g, jval_s(p->val));
   v = (vertex)jval_v(find->val);
   find2 = jrb_find_str(v->tree, jval_s(q->val));
   d2 = (Dllist)jval_v(find2->val);
   dll_traverse(node,d2)
      {
	printf("%d ", jval_i(node->val));
        so2chu(jval_i(node->val), chu);
        strcat(ketqua, chu);
        strcat(ketqua, " ");
      }
   printf("\n");
   strcat(ketqua, "\n");
   p = q;
 }

//thuc hien chuc nang 3
 
 taomangzero(A);
 taomangzero(B);
 taomangzero(C);
 p = dll_first(d);
 
   q = dll_next(p);
   if(q==dll_last(d)) {  gtk_text_buffer_set_text(buffer, ketqua, -1); return;}
   printf("ban nen di theo lo trinh sau de it phai doi xe nhat \n");
   strcat(ketqua, "\nSuggest way:  \n");
   k = dll_next(q);

   find = jrb_find_str(g, jval_s(p->val));
   v = (vertex)jval_v(find->val);
   find2 = jrb_find_str(v->tree, jval_s(q->val));
   d2 = (Dllist)jval_v(find2->val);
   taomangzero(A);
   dll_traverse(node,d2)
   {
     A[jval_i(node->val)] = 1;
    
   }
   for(;;)
     {
   printf("- tu %s den ", jval_s(p->val));
    strcat(ketqua, "+ ");
   strcat(ketqua, jval_s(p->val));
   strcat(ketqua, " --> ");
  
       for(;;)
        {
   
	  find = jrb_find_str(g, jval_s(q->val));
	  v = (vertex)jval_v(find->val);
	  find2 = jrb_find_str(v->tree, jval_s(k->val));
	  d2 = (Dllist)jval_v(find2->val);
	  taomangzero(B);
          dll_traverse(node,d2)
          {
             B[jval_i(node->val)] = 1;
	   
	  }
	
	  tronmang(A,B,C);
	 
           if(mangzero(C))
	     {
	       printf(" %s : go by Bus.No %d\n", jval_s(q->val), timnhonhat(A));
               strcat(ketqua, jval_s(q->val));
               strcat(ketqua, " : go by Bus.No: ");
               so2chu(timnhonhat(A), chu);
               strcat(ketqua, chu);
               strcat(ketqua, "\n");
	       p = q;
	       q = k;
	       if(q == dll_last(d)) 
		 {
		   printf("+ %s --> %s : go by Bus.No: %d\n", jval_s(p->val), jval_s(q->val), timnhonhat(B));
                   strcat(ketqua, "+ ");
                   strcat(ketqua, jval_s(p->val));
                   strcat(ketqua, " --> ");
                   strcat(ketqua, jval_s(q->val));
                   strcat(ketqua, " : go by Bus.No: ");
                   so2chu(timnhonhat(B), chu);
                   strcat(ketqua, chu);
                   strcat(ketqua, "\n");
                   gtk_text_buffer_set_text(buffer, ketqua, -1);
		   return;
		 }
	       k = dll_next(q);
	       copymang(A,B);
	       break;
	     }
	   else
	     {
	       p = q;
	       q = k;
	       if(q == dll_last(d)) 
		 {
		   printf("%s : go by Bus.No %d\n", jval_s(q->val), timnhonhat(C));
                   strcat(ketqua, jval_s(q->val));
                   strcat(ketqua, " : go by Bus.No ");
                   so2chu(timnhonhat(C), chu);
                   strcat(ketqua, chu);
                   strcat(ketqua, "\n");
                   gtk_text_buffer_set_text(buffer, ketqua, -1);
		   return;
		 }
	       k = dll_next(q);
	       copymang(B,C);
	       copymang(A,B);
	     }
	    
	}
      
 }
  
 
}


//cac ham cho gtk
static void enter_entry2( GtkWidget *widget, GtkWidget *entry )
{
  int l;
  char *entry_text2;
  char *entry_text1;

  entry_text2 = gtk_entry_get_text (GTK_ENTRY (entry2));
  entry_text1 = gtk_entry_get_text (GTK_ENTRY (entry1));
   
  path(graph, entry_text1, entry_text2);
}

static void enter_entry4( GtkWidget *widget, GtkWidget *entry )
{
  char *entry_text, ketqua[3000];
  JRB find, find2, node;
  vertex v;
  Dllist node2, d;
  char chu[4];

  entry_text = gtk_entry_get_text (GTK_ENTRY (entry4));
  strcpy(ketqua, " The next bus stop from ");
  strcat(ketqua, entry_text);
  strcat(ketqua, " is: \n\n ");
  printf("noi dung %s\n", ketqua);
 
  find = jrb_find_str(graph, entry_text);
  if(find==NULL) 
  {
      strcat(ketqua, "No have place: ");
      strcat(ketqua, entry_text);
      strcat(ketqua, " in the map.");
      strcat(ketqua, " \n");
      gtk_text_buffer_set_text(buffer, ketqua, -1);
      return;
  }
  v = (vertex)jval_v(find->val);
  jrb_traverse(node, v->tree)
  {   strcat(ketqua, "+ ");
      strcat(ketqua, jval_s(node->key));
      strcat(ketqua, " : go by Bus.No: ");
      d = (Dllist)jval_v(node->val);
      dll_traverse(node2, d)
      {
            so2chu(jval_i(node2->val), chu);
            strcat(ketqua, chu);
            strcat(ketqua, " ");
      }
      strcat(ketqua, "\n ");
  }
  strcat(ketqua, "\n");
  gtk_text_buffer_set_text(buffer, ketqua, -1);
  
}

static void enter_entry3( GtkWidget *widget, GtkWidget *entry )
{
  char *entry_text, ketqua[4000];
  JRB find, find2, node;
  vertex v;
  
  strcpy(ketqua,"");
  entry_text = gtk_entry_get_text (GTK_ENTRY (entry3));
  if(atoi(entry_text)<=0 || atoi(entry_text)>60)  {gtk_text_buffer_set_text(buffer, "Not have Bus.No ", -1); return;}
  inlotrinhtuyen(atoi(entry_text), ketqua);
  gtk_text_buffer_set_text(buffer, ketqua, -1);
  
}


int main( int argc, char **argv )
{
   GtkBuilder *builder;
   GError     *error = NULL;
   GtkWidget *button1;
   GtkWidget *window;

 //  Graph graph;
   char start[30], stop[30];
 
    /* Init GTK+ */
    gtk_init( &argc, &argv );
  
    
   

    graph =  createGraph();
   // inlotrinhtuyen(1);
 
    builder = gtk_builder_new();
    gtk_builder_add_from_file( builder, "hnbus2.glade", &error ) ;

    //lay doi tuong window tu builder
    window = GTK_WIDGET (gtk_builder_get_object (builder, "window1"));
    textview1 = GTK_WIDGET (gtk_builder_get_object (builder, "textview1"));
    entry1 = GTK_WIDGET (gtk_builder_get_object (builder, "entry1"));
    entry2 = GTK_WIDGET (gtk_builder_get_object (builder, "entry2"));
    entry4 = GTK_WIDGET (gtk_builder_get_object (builder, "entry4"));
    entry3 = GTK_WIDGET (gtk_builder_get_object (builder, "entry3"));
    button1 = GTK_WIDGET (gtk_builder_get_object (builder, "button1"));
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview1));
   
    gtk_window_move (window, 300, 60);
    g_signal_connect (entry2, "activate", G_CALLBACK (enter_entry2), (gpointer) entry2);   
    g_signal_connect (entry1, "activate", G_CALLBACK (enter_entry2), (gpointer) entry1);   
    g_signal_connect (entry4, "activate", G_CALLBACK (enter_entry4), (gpointer) entry4);  
    g_signal_connect (entry3, "activate", G_CALLBACK (enter_entry3), (gpointer) entry3);    
    button1 =  GTK_WIDGET (gtk_builder_get_object (builder, "button1"));      //nut Close
    g_signal_connect (G_OBJECT(button1), "clicked", G_CALLBACK(enter_entry2), buffer);

    g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (on_window_destroy), NULL);/*ket thuc chuong trinh*/
    
    gtk_widget_show_all(window);
    
    gtk_main();
    return 0;
}
