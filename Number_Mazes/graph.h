#ifndef GRAPH_H
#define GRAPH_H

#include<stdbool.h>
#include "list.h"
#include "queue.h"

typedef list *graph;
graph graph_empty(int n);
void graph_free(graph g, int n);
bool graph_reach(graph g, int n, int i, int j);
void graph_shortest_path(graph g, int n, int start, int end, int row);

# endif // GRAPH_H
