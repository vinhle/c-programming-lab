Student Name: LE VINH



Project 1 - I117 Programming Laboratory II

 (7/2015): Number Maze Game

1. Introduction

Number mazes slover is written in C, by encoding mazes[n,m] into graph which has n*m nodes.

Graph is presented by adjacency list. 
The function of program
:
- Allow users input a file name
- Read & check this file and convert to graph

- Let user know the maze can be sloved or not

- Show the shortest solution (minimum of move steps)


2. How to run the program
I created a makefile to make easier in writting commands

- Compile by command: make

- Excecute file by command: ./numberMaze

