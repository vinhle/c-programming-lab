/* LE VINH (1410212) - JAIST
Main file of program
- Control the inputs from user
- Execute commands from the user choice
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "graph.h"
#define MAX_FILENAME 100

// Convert from index i, j in matrix to node (the index of node in graph)
int iton(int row, int col, int i, int j)
{
    return i*col + j;
}

graph readFile(char *fname, int *size, int *goal, int *c)
{
    char ch, tmp[3];
    int i=0, j=0, row, col;

    FILE *fp = fopen(fname, "r");
    if(fp == NULL) {
        printf("Can not open file %s or there is no file %s !\n", fname, fname);
        return NULL;
    }

    // Read 2 first lines of file
    while (i < 2)  {
        fscanf(fp, "%s", tmp); // The number may be > 9
        if(atoi(tmp) != 0) {
            if(i == 0) { row = atoi(tmp); printf("The number of rows in maze: %d\n", row); }
            if(i == 1) { col = atoi(tmp); printf("The number of columns in maze: %d\n", col); }
        }
        else {
            printf("\nError format file, the first/second line must be a number! \n");
            return NULL;
        }
        i++;
    }
    // Read the remaining of file
    int maze[row][col];
    i = 0; j = 0;
    ch = fgetc(fp);
    while((ch = fgetc(fp)) != EOF) {
        if(ch == '\n') {
            i++;
            if(j != col) { printf("\nError the number of columns !\n"); return NULL; }
            j = 0;
        }
        else {
            if(ch != 'G') maze[i][j] = ch - '0';
            else maze[i][j] = 0;
            j++;
        }
        printf(" %c",ch);
    }
    if(i != row) { printf("\nError the number of rows !\n"); return NULL; }
    fclose(fp);

    /*printf("Matrix: \n");
    for(i=0; i<row; ++i) {
        for(j=0; j<col; ++j) printf("%d ", maze[i][j]);
        printf("\n"); }*/
    // Make graph from matrix
    int val, node;
    (*size) = row * col;
    (*c)    = col;
    graph g = malloc(sizeof(list) * row * col);
    for(i=0; i<(*size); i++) g[i] = NULL;
    for(i=0; i<row; i++) {
        for(j=0; j<col; j++) {
            val  = maze[i][j];
            if(val != 0) {
                node = iton(row, col, i, j);
                if(j+val < col) list_cons(iton(row, col, i, j+val), &g[node]);
                if(j-val >= 0)  list_cons(iton(row, col, i, j-val), &g[node]);
                if(i+val < row) list_cons(iton(row, col, i+val, j), &g[node]);
                if(i-val >= 0)  list_cons(iton(row, col, i-val, j), &g[node]);
            }
            else (*goal) = iton(row, col, i, j);
        }
    }
    //for(i=0; i<(*size); i++) {printf("Edges from node %d to ", i); list_print(g[i]);}
    return g;
}

int main()
{
    char fname[MAX_FILENAME], choice;
    int col=0, size=0, goal=0, kt=0;
    graph g=NULL, tmp=NULL;

    printf("***** Welcome to Number Maze solver ***** \n");
    printf("+ 1. Input 1 to enter your number maze file name and print the maze \n");
    printf("+ 2. Input 2 to check whether your maze can be solved or not \n");
    printf("+ 3. Input 3 to show the shorest solution \n");
    printf("+ 4. Input any other character to quit the program \n");
    while(1)
    {
        printf("> Input your choice: "); scanf("%c", &choice);
        while(getchar()!='\n');
        switch(choice)
        {
            case '1':
                printf("Please write the name of your input file: ");
                scanf("%s", fname);
                while(getchar()!='\n');
                tmp = readFile(fname, &size, &goal, &col);
                if(tmp != NULL) {
                    graph_free(g, kt);
                    kt = size; g = tmp;
                }
                //printf("Size %d, Goal: %d \n", size, goal);
                break;
            case '2':
                if(g != NULL) {
                    if(graph_reach(g, size, 0, goal) == true) printf("This maze is solvable. Input 3 to see the solution.\n");
                    else printf("This maze is unsolvable (noway to solve). \n");
                }
                else printf("No input file so far. \n");
                break;
            case '3':
                // Finding the shortest path between two nodes u and v (Directed unweighted graph) by modified BFS
                if(g != NULL) graph_shortest_path(g, size, 0, goal, col);
                else printf("No input file so far. \n");
                break;
            default:
                if(g != NULL) graph_free(g, size);
                printf("Close Number Maze solver. See you! \n");
                return 0;
        }
    }
}
