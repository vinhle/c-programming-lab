/* LE VINH (1410212) - JAIST
Graph library
- Contains function about graph
- Most important functions
    + reach: to check whether from node i can reach node j = the maze can be solved or not
    + graph_shortest_path: show the shorest path from node i to j = the minimum move step to solve the maze
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "graph.h"

/* Graph is presented by adjacency list */
typedef list *graph;

// Make graph empty
graph graph_empty(int n)
{
    // Array g contains the elements that is in the type of pointer to list
    graph g = malloc(sizeof(list) * n);
    int i;
    for(i=0; i<n; i++) g[i] = NULL;
    return g;
}

void graph_free(graph g, int n)
{
    int i;
    for(i=0; i<n; i++) list_free(g[i]);
    free(g);
}

// Graph reachability problem, check if there is a path from i to j in the graph g
// n is the number of nodes, i and j should be in (0, n-1)
bool reachAux(graph g, int n, int i, int j, bool *visited)
{
    if(i == j) return true;
    visited[i] = true;
    int k;
    for(k=0; k<n; k++)
        if(!visited[k] && list_member(k, g[i]))
            if(reachAux(g,n,k,j,visited)) return true;
    return false;
}
bool graph_reach(graph g, int n, int i, int j)
{
    bool visited[n];
    int k;
    for(k=0; k<n; k++) visited[k] = false;
    return reachAux(g, n, i, j, visited);
}

// Find the shorest path from node i to node j in unweighted graph by modified BFS algorithm
void graph_shortest_path(graph g, int n, int start, int end, int col)
{
    int k, distance=0;
    bool visited[n];
    for(k=0; k<n; k++) visited[k] = false;

    int prev[n]; // Store the visited path
    for(k=0; k<n; k++) prev[k] = -1;
    prev[start] = start;

    queue q = malloc(sizeof(*q)); // elements of queue are in the type of list (pointer to node)
    queue_init(q);
    queue_enqueue(q, start);
    while(!queue_isempty(q))
    {
        int current = queue_dequeue(q);
        if(visited[current]) continue;
        //printf("%d ", current);
        visited[current] = true;
        list neigh = g[current];
        while(neigh != NULL)
        {
            if(!visited[neigh->x]) queue_enqueue(q, neigh->x);
            if(prev[neigh->x] == -1) prev[neigh->x] = current;
            neigh = neigh->next;
        }
    }
    free(q);

    //for(k=0; k<n; k++) printf("The previous node %d is node %d \n", k, prev[k]);
    k = end;
    int dis = 0; // the number of node from start to end
    while(1) {
        if(k == -1)    { dis = -1; break; }
        if(k == start) { dis++; break; }
        dis++;
        k = prev[k];
    }
    if(dis == -1) { printf("This maze is unsolvable (noway to solve). \n"); return;}
    else
    {
        int path[dis], i = dis-1;
        k = end;
        while(1) {
            path[i] = k; i--;
            if(k == start) break;
            k = prev[k];
        }

        /*Print shorest path by graph node
        printf("The shortest path from node %d to %d is ", start, end);
        for(i=0; i<dis; i++) printf("%d ", path[i]);
        printf("\n");*/

        // Convert to squenence of move steps in maze game
        printf("The shortest soltuion (minimum number of move steps) for this maze: \n\t");
        for(i=1; i<dis; i++) {
            if(path[i] > path[i-1]) {
                if(path[i] - path[i-1] < col) printf("R ");
                else printf("D ");
            }
            if(path[i] < path[i-1]) {
                if(path[i-1] - path[i] < col) printf("L ");
                else printf("U ");
            }
        }
        if(dis == 1) printf("Don't need move since the goal is start position.");
        printf("\n");
    }
    return;
}
