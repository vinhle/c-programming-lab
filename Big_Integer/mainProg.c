#include<stdio.h>
#include<stdlib.h>
#include"bigint.h"

int main()
{
    char choice;
    int a, b;
    bigint x, y, z;
    printf("***** Welcome to Demo Program for Big Integer Library ***** \n");
    printf("+ 1. Input 1 to convert a integer to bigint number and print it again \n");
    printf("+ 2. Input 2 to compute the addition of two bigints \n");
    printf("+ 3. Input 3 to compute the multiplication of two bigints \n");
    printf("+ 4. Input 4 to compute factorial of n! \n");
    printf("+ 5. Input 5 to compute fibonacci number at n(th) \n");
    printf("+ 6. Input any other character to quit the program \n");
    printf("Enter to continue!\n");
    while(1)
    {
        while(getchar()!='\n');
        printf("> Input your choice: "); scanf("%c", &choice);
        while(getchar()!='\n');
        switch(choice)
        {
            case '1':
                printf("Input your integer: "); scanf("%d", &a);
                x = convert(a);
                printf("Your bigint: "); print_bigint(x);
                free_bigint(x);
                break;
            case '2':
                printf("Input your integer a: "); scanf("%d", &a);
                printf("Input your integer b: "); scanf("%d", &b);
                x = convert(a);
                y = convert(b);
                z = add(x,y);
                printf("bigint addition a + b = "); print_bigint(z);
                free_bigint(x); free_bigint(y); free_bigint(z);
                break;
            case '3':
                printf("Input your integer a: "); scanf("%d", &a);
                printf("Input your integer b: "); scanf("%d", &b);
                x = convert(a);
                y = convert(b);
                z = mult(x,y);
                printf("bigint multiplication a * b = "); print_bigint(z);
                free_bigint(x); free_bigint(y); free_bigint(z);
                break;
            case '4':
                printf("Compute fatorial of "); scanf("%d", &a);
                x = fact(a);
                print_bigint(x);
                free_bigint(x);
                break;
            case '5':
                printf("Compute fibonacci at "); scanf("%d", &a);
                x = fib(a);
                print_bigint(x);
                free_bigint(x);
                break;
            default:
                printf("Close the program. Good bye! \n");
                return 0;
        }
    }
}
