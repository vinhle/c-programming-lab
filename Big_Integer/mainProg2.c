#include<stdio.h>
#include<stdlib.h>
#include"bigint.h"

// Check memory leak
int main()
{
    bigint a = fib(1000) ;
    print_bigint(a) ;
    bigint b = fact(100) ; // or even fact (1000)
    print_bigint(b) ;
    free_bigint(a); free_bigint(b);

    while (1) {
        a = fact(100) ;
        free_bigint(a) ;
    }

    return 0;
}
