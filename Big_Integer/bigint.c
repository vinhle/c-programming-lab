#include<stdio.h>
#include<stdlib.h>
#include"bigint.h"

#define	MAXDIGITS 20		/* maximum length of long int(19 bits) */

// Utilities in bigint
bigint convert(int x)   // Assumption x always >= 0
{
    bigint bi;
    if(x < 0) { bi.array = NULL; bi.n = 0; }
    else
        if(x == 0) {
            bi.array = malloc(sizeof(int));
            bi.array[0] = 0; bi.n = 1;
        }
        else {
            int i, n = 0;
            int tmp[MAXDIGITS];
            while(x > 0) {
                tmp[n] = x % BASE;
                n++;
                x = x / BASE;
            }
            bi.array = malloc(sizeof(int) * n);
            for(i=0; i<n; i++) bi.array[i] = tmp[i];
            bi.n = n;
        }
    return bi;
}

void print_bigint(bigint a) {
    int i, n;
    n = a.n;
    if(n == 0) {printf("No possible number\n"); return;}
    printf("%d", a.array[n-1]);
	for(i = n-2; i>=0; i--)
	{
        if(a.array[i] < 10) printf("00%d", a.array[i]);
        else if(a.array[i] < 100) printf("0%d", a.array[i]);
        else if(a.array[i] < 1000) printf("%d", a.array[i]);
	}
	printf("\n");
}
void print_struct(bigint a) {
    printf("Bigint: "); print_bigint(a);
    printf("> Array (%d elements): ", a.n);
    int i; for(i=0; i<a.n; i++) printf("%d ", a.array[i]);
    printf("\n");
}


void free_bigint(bigint a) {
    free(a.array);
    a.n = 0;
}

// Operation in bigint
/* a + b */
bigint add(bigint a, bigint b) {
    if(a.n==0 || b.n==0) { return convert(-1); }
    int size = a.n;
    if(a.n < b.n) size = b.n;

    int i, carry, tmp, c[size+1];
    carry = 0; // no carry
    for(i=0; i<size; i++) {
        if(i >= a.n) tmp = b.array[i] + carry;      // the remaining digits of b, if a.n < b.n
        else if(i >= b.n) tmp = a.array[i] + carry; // the remaining digits of a, if a.n > b.n
             else tmp = a.array[i] + b.array[i] + carry;
        if(tmp >= BASE) {
            carry = 1;
            tmp  -= BASE;
        }
        else carry = 0;
		c[i] = tmp;
    }
    if (carry == 1) {c[i] = 1; i++;}
    size = i;

    bigint sum;
    sum.array = malloc(sizeof(int) * size); sum.n = size;
    for(i=0; i<size; i++) sum.array[i] = c[i];
    return sum;
}

// a * b
bigint mult_one(bigint a, int m) { // a * m
    int size = a.n, carry = 0;
    int i, tmp[a.n+3];
    for(i=0; i<a.n; i++) {
        tmp[i] = m * a.array[i] + carry;
        if(tmp[i] >= BASE) {
            carry  = tmp[i] / BASE;
			tmp[i] = tmp[i] % BASE;
        }
        else carry = 0;
    }
    if (carry != 0) { tmp[i] = carry; i++; }
    size = i;

    bigint mul_one;
    mul_one.array = malloc(sizeof(int) * size); mul_one.n = size;
    for(i=0; i<size; i++) mul_one.array[i] = tmp[i];
    return mul_one;
}
bigint shift_left(bigint a, int m) { // multiplies a number by BASE^m
	int	i, size = a.n + m;
	// move elements to the left n spaces and the remaining is filled by zeros
	bigint lshift;
    lshift.array = malloc(sizeof(int) * size); lshift.n = size;
    for(i=0; i<m; i++)   lshift.array[i] = 0;
    for(i=0; i<a.n; i++) lshift.array[i+m] = a.array[i];
    return lshift;
}
bigint mult(bigint a, bigint b) {
    if(a.n==0 || b.n==0) { return convert(-1); }
    bigint tmp1, tmp2, tmp3;                // a partial products
    bigint mul = convert(0);                // The sum of partial products
    tmp3 = mul;
    int i;
    for(i=0; i<b.n; i++) {
        tmp1 = mult_one(a, b.array[i]);     // multiply a by b[i]
        tmp2 = shift_left(tmp1, i);         // shift the partial product left i bytes
        mul  = add(tmp3, tmp2);
        free_bigint(tmp1);
        free_bigint(tmp2);
        free_bigint(tmp3); tmp3 = mul;
    }
    return mul;
}

// Compute factorial x! by dynamic programming
bigint fact(int x) {
    if(x < 0)  return convert(-1);
    if(x == 0) return convert(1);

    bigint fact[x+1], tmp;
    int i;
    fact[0] = convert(1);
    for(i=1; i<=x; i++) {
        tmp = convert(i);
        fact[i] = mult(tmp,fact[i-1]);
        free_bigint(tmp);
    }

    //for(i=0; i<x+1; i++) { printf("fact[%d] = ",i); print_bigint(fact[i]); }
    for(i=0; i<x; i++) free_bigint(fact[i]);
    return fact[x];
}

// Compute find the fibonacci number x(th) by dynamic programming
bigint fib(int x) {
    if(x < 0)  return convert(-1);
    if(x == 0) return convert(0);

    bigint fib[x+1];
    int i;
    fib[0] = convert(0);
    fib[1] = convert(1);
    for(i=2; i<=x; i++) fib[i] = add(fib[i-2],fib[i-1]);

    //for(i=0; i<x+1; i++) { printf("fib[%d] = ",i); print_bigint(fib[i]); }
    for(i=0; i<x; i++) free_bigint(fib[i]);
    return fib[x];
}
