//Lap Circular Linked List so dia chi dien thoai
//Mot hoat dong CList tu node thu hai tro di
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct ele_type
{
  char name[50];
  char tel[15];
  char email[30];
}ele_type;
typedef struct node
{
  struct ele_type addr;
  struct node *next;
}node;
typedef node CList;

void Make_null(CList **root)//Tao mot node dau rong thong tin
{
    CList *tmp;
    tmp = (CList *)malloc(sizeof(CList));
    if(tmp == NULL)
    {
      printf("Memory allocation failed!\n");
      exit(1);
    }
    tmp->next = tmp;
    (*root) = tmp;
}
void New_address(CList **root, CList **cur, ele_type e)//Them 1 dia chi vao cuoi
{
  CList *new;
  new = (CList *)malloc(sizeof(CList));
  if(new == NULL)
    {
      printf("Memory allocation failed!\n");
      exit(1);
    }
  new->addr = e;
  new->next = (*root);
  (*cur)->next = new;
  (*cur) = (*cur)->next;
}

int Count_address(CList *root)//count number of Double List
{
  int count = 1;
  if(root == NULL) return 0;
  CList *tmp = root->next;
  while(tmp != root)
    {
      count++;
      tmp = tmp->next;
    }
  return count;
}

void Insert_address(CList *root)//CList chi co chen dia chi vao giua
{
  int n, i=0;
  CList *curr_addr = NULL, *add = NULL;
  printf("Chen vao vi tri thu: "); scanf("%d", &n);
  if(n > Count_address(root)|| n < 0)
  {
      printf("So ban nhap khong ton tai\n");
      return;
  }
  add = (CList *)malloc(sizeof(CList));
  if (add == NULL)
      {
          printf("Memory allocation failed!\n");
          exit(1);
      }
  while(getchar()!='\n');
  printf("Name      : "); gets(add->addr.name);
  printf("Telephone : "); gets(add->addr.tel);
  printf("Email     : "); gets(add->addr.email);
  if(n == 0)//Adding a ele_type - beginning
    {
      add->next = root->next;
      root->next = add;
      return;
    }
  //Adding a ele_type - mid/end
  curr_addr = root->next;
  while(curr_addr != root && i < n-1)
    {
      curr_addr = curr_addr->next;
      i++;
    }
  add->next = curr_addr->next;
  curr_addr->next = add;
}

void Remove_address(CList **root)//Chi co xoa o giua khong phai xet dau-cuoi
{
  CList *curr_addr = NULL, *prev_addr=NULL;
  char ten[50];
  while(getchar()!='\n');
  printf("Ten cua nguoi can xoa: ");
  gets(ten);
  if((*root) == NULL)
  {
      printf("Danh sach rong\n");
      return;
  }
  curr_addr = (*root)->next;
  //remove a ele_type - beginning
  if(strcmp(curr_addr->addr.name, ten) == 0)
    {
      (*root)->next = curr_addr->next;
      free(curr_addr);
      return;
    }
  //remove a ele_type - mid list
  while(curr_addr != (*root) && strcmp(curr_addr->addr.name, ten) != 0)
  {
    prev_addr = curr_addr;
    curr_addr = curr_addr->next;
     //De phong truong hop khong tim thay dan toi lap vo han
  }
  if(curr_addr != (*root))
    {
      prev_addr->next = curr_addr->next;
      free(curr_addr);
    }
  else printf("Khong tim thay trong danh sach\n");
}

void Enter(CList **root, CList **cur) //Ham nhap cac phan tu cua danh sach lien ket don
{
  int i=0;
  ele_type tmp;
  printf("Nhap danh sach (ten 0 de ket thuc) : \n");
  do
    {
      printf("Person  %d:\n",i++);
      printf("\tName      : "); gets(tmp.name);
      if(strcmp(tmp.name,"0") == 0) break;
      printf("\tTelephone : "); gets(tmp.tel);
      printf("\tEmail     : "); gets(tmp.email);
      New_address(root, cur, tmp);
    } while(1);
}

void Print_list(CList *root)//Print all ele_type in phone book
{
  CList *tmp;
  tmp = root->next;
  int i = 0;
  while (tmp != root)
    {
      printf("Nguoi thu %d:\n",i++);
      printf("\t %s  %s  %s \n", tmp->addr.name, tmp->addr.tel, tmp->addr.email);
      tmp = tmp->next;
    }
  printf("\n");
}

int main()
{
  CList *root = NULL, *cur = NULL;
  Make_null(&root);
  cur = root;
  CList *tmp;
  int choice;
  Enter(&root, &cur);
  do
    {
      printf("(1)Insert (2)Remove (0)Exit\n");
      printf("Your choice is "); scanf("%d",&choice);
      switch(choice)
      {
        case 1:Insert_address(root);
          printf("\nAfter insert, phone address: \n");
          Print_list(root);
          break;
        case 2:Remove_address(&root);
          printf("\nAfter remove, phone address: \n");
          Print_list(root);
      }
    }while(choice!=0);
  printf("Number of ele_type in double list: %d\n",Count_address(root));
  tmp = root;
  while (tmp != root)
    {
      tmp = root->next;
      free(tmp);
      tmp = root;
    }
  free(root);
  return 0;
}
