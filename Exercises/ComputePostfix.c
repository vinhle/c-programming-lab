//Tinh gia tri cua mot bieu thuc hau to
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef int ele_type;
typedef struct node
{
  ele_type data;
  struct node *link;
}stack;

int Is_empty(stack *top)
{
  return (top -> link == NULL);
}

void Push(stack **top, ele_type value)
{
  stack *temp;
  temp = (stack *) malloc(sizeof(stack));
  if (temp == NULL)
    {
      printf("No memory available Error\n");
      exit(0);
  }
  temp->data = value;
  temp->link = (*top);
  (*top) = temp;
}

ele_type Pop(stack **top)
{
  struct node *temp;
  ele_type value;
  if(top == NULL)
    {
      printf("The stack is empty can not Pop Error\n");
      exit(0);
    }
  value = (*top)->data;
  temp = (*top);
  (*top) =(*top)->link;
  free(temp);
  return value;
}

int Tinh_bieuthuc(char *str, stack **top)
{
    int i=0, j=0, tmp1, tmp2, tmp3;
    char tmp[10];
    while(i<strlen(str))
    {
        if(str[i]>='0' && str[i]<='9')
        {
            while(str[i]>='0' && str[i]<='9')//Dung vong lap vi mot so co nhieu chu so
            {
                tmp[j++] = str[i];
                i++;
            }
            tmp[j] = '\0';
            tmp3 = atoi(tmp);
            Push(top, tmp3);
        }
        j = 0;
        if(str[i]=='+' || str[i]=='-' || str[i]=='*' || str[i]=='/')
        {
            tmp1 = Pop(top);
            tmp2 = Pop(top);
            if(str[i]=='+') tmp3 = tmp1 + tmp2;
            if(str[i]=='-') tmp3 = tmp1 - tmp2;
            if(str[i]=='*') tmp3 = tmp1 * tmp2;
            if(str[i]=='/') tmp3 = tmp1 / tmp2;
            Push(top, tmp3);
        }
        i++;
    }
    return Pop(top);
}

int main()
{
    char str[100];
    stack *top = NULL;
    printf("Nhap vao bieu thuc hau to: ");
    gets(str);
    printf("Gia tri phep tinh hau to : %d\n", Tinh_bieuthuc(str, &top));
    return 0;
}
