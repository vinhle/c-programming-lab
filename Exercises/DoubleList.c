//Lap Double List so dia chi dien thoai
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct ele_type
{
  char name[50];
  char tel[15];
  char email[30];
}ele_type;
typedef struct node
{
  struct ele_type addr;
  struct node *prev;
  struct node *next;
}node;
typedef node DList;

void New_address(DList **root, DList **cur, ele_type e)//Them 1 dia chi vao cuoi
{
  DList *new;
  new = (DList *)malloc(sizeof(DList));
  if(new == NULL)
    {
      printf("Memory allocation failed!\n");
      exit(1);
    }
  new->addr = e;
  new->prev = NULL;
  new->next = NULL;
  if(*root == NULL)
    {
      (*root) = new;
      (*cur) = (*root);
    }else
       {
        (*cur)->next = new;
        new->prev = (*cur);
        (*cur) = (*cur)->next;
       }
}

int Count_address(DList *root)//count number of Double List
{
  int count = 0;
  DList *tmp = root;
  while(tmp != NULL)
    {
      count++;
      tmp = tmp->next;
    }
  return count;
}

void Insert_address(DList **root)//Chen 1 dia chi
{
  int n, i=0;
  DList *curr_addr = NULL, *add = NULL;
  printf("Chen vao vi tri thu: "); scanf("%d", &n);
  if(n > Count_address(*root)|| n < 0)
  {
    printf("So ban nhap khong ton tai\n");
    return 0;
  }
  add = (DList *)malloc(sizeof(DList));
  if (add == NULL)
  {
    printf("Memory allocation failed!\n");
    exit(1);
  }
  while(getchar()!='\n');
  printf("Name      : "); gets(add->addr.name);
  printf("Telephone : "); gets(add->addr.tel);
  printf("Email     : "); gets(add->addr.email);
  add->prev = NULL;
  add->next = NULL;
  if(*root == NULL)
  {
    (*root) = add;
    return 0;
  }
  if(n == 0)//Adding a Address - beginning
  {
    add->next = (*root);
    (*root)->prev = add;
    (*root) = add;
    return 0;
  }
  curr_addr = (*root);//Adding a ele_type - mid/end
  while(curr_addr != NULL && i < n-1)
    {
      curr_addr = curr_addr->next;
      i++;
    }
  add->next = curr_addr->next;
  add->prev = curr_addr;
  curr_addr->next->prev = add;
  curr_addr->next = add;
}

void Remove_address(DList **root)
{
  DList *curr_addr = NULL, *prev_addr = NULL;
  char ten[50];
  while(getchar()!='\n');
  printf("Ten cua nguoi can xoa: ");
  gets(ten);
  if((*root) == NULL) //remove a address - beginning
  {
      printf("Danh sach rong\n");
      return 0;
  }
  curr_addr = (*root);
  if(strcmp(curr_addr->addr.name, ten) == 0)
    {
      (*root) = (*root)->next;
      (*root)->prev = NULL;
      free(curr_addr);
    }
  //remove a ele_type - mid list
  while(curr_addr != NULL && strcmp(curr_addr->addr.name, ten) != 0)
    {
      prev_addr = curr_addr;
      curr_addr = curr_addr->next;
    }
  if(curr_addr != NULL)
    {
      if(curr_addr->next != NULL)//Xoa phan tu giua danh sach
      {
          prev_addr->next = curr_addr->next;
          curr_addr->next->prev = prev_addr;
          free(curr_addr);
      }
      else//Xoa phan tu cuoi danh sach
      {
          prev_addr->next = NULL;
          free(curr_addr);
      }
    }
  else printf("Khong tim thay trong danh sach\n");
}

void Enter(DList **root, DList **cur) //Ham nhap cac phan tu cua danh sach lien ket don
{
  int i=0;
  ele_type tmp;
  printf("Nhap danh sach (ten 0 de ket thuc) : \n");
  do
    {
      printf("Person  %d:\n",i++);
      printf("\tName      : "); gets(tmp.name);
      if(strcmp(tmp.name,"0") == 0) break;
      printf("\tTelephone : "); gets(tmp.tel);
      printf("\tEmail     : "); gets(tmp.email);
      New_address(root, cur, tmp);
    } while(1);
}

void Print_list(DList *root)//Print all ele_type in phone book
{
  DList *tmp;
  tmp = root;
  int i = 0;
  while (tmp != NULL)
    {
      printf("Nguoi thu %d:\n",i++);
      printf("\t %s  %s  %s \n", tmp->addr.name, tmp->addr.tel, tmp->addr.email);
      tmp = tmp->next;
    }
  printf("\n");
}

int main()
{
  DList *root = NULL, *cur = NULL;
  DList *tmp;
  int choice;
  Enter(&root, &cur);
  do
    {
      printf("(1)Insert (2)Remove (0)Exit\n");
      printf("Your choice is "); scanf("%d",&choice);
      switch(choice)
      {
        case 1:Insert_address(&root);
          printf("\nAfter insert, phone address: \n");
          Print_list(root);
          break;
        case 2:Remove_address(&root);
          printf("\nAfter insert, phone address: \n");
          Print_list(root);
      }
    }while(choice!=0);
  printf("Number of ele_type in double list: %d\n",Count_address(root));
  tmp = root;
  while (tmp != NULL)
    {
      root = root->next;
      free(tmp);
      tmp = root;
    }
  return 0;
}
