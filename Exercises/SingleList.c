//Quan ly sinh vien bang danh sach lien ket don
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define ID_LENGTH 8
#define NAME_LENGTH 50
typedef struct Student
{
  char ID[ID_LENGTH];
  char name[NAME_LENGTH];
  int grade;
}Student;
typedef struct StudentList
{
  Student std;
  struct StudentList *next;
}StudentList;
StudentList *root = NULL, *cur = NULL;

StudentList *new_student(Student node)//Cap them 1 hoc sinh vao cuoi
{
  StudentList *new;
  new = (StudentList *)malloc(sizeof(StudentList));
  if(new == NULL)
    {
      printf("Memory allocation failed\n");
      exit(1);
    }
  new->std = node;
  new->next = NULL;
  if(root == NULL)
    {
      root = new;
      cur = root;
    }else
      {
        cur-> next = new;
        cur = cur -> next;
      }
  return new;
}

int count_student()//Dem so hoc sinh trong danh sach
{
  int count = 0;
  StudentList *tmp = root;
  while(tmp!=NULL)
    {
      tmp = tmp->next;
      count++;
    }
  return count;
}

StudentList *find_stduent()
{
  StudentList *tmp;
  char SHSV[ID_LENGTH];
  printf("So hieu sinh vien nguoi can tim: ");
  gets(SHSV);
  tmp = root;
  while(tmp!=NULL)
    {
      if(strcmp(tmp->std.ID, SHSV) == 0) return tmp;
      tmp = tmp->next;
    }
  return NULL;
}

StudentList *change_grade()
{
  int diem;
  StudentList *tmp;
  tmp = find_stduent();
  if(tmp != NULL)
    {
      printf("Thong tin hien tai cua sinh vien: ");
      printf("\t%s %s %d\n", tmp->std.ID, tmp->std.name, tmp->std.grade);
      printf("Cap nhat diem cua sinh vien: ");
      scanf("%d",&diem);
      tmp->std.grade = diem;
    }
  return tmp;
}

StudentList *insert_student()
{
  StudentList *curr_std=NULL, *prev_std=NULL, *add=NULL;
  add = (StudentList *)malloc(sizeof(StudentList));
  if(add == NULL)
    {
      printf("Memory allocation failed\n");
      exit(1);
    }
  printf("The list is sorted on descending order of student's grade\n");
  printf("New student: \n");
  printf("ID    :"); gets(add->std.ID);
  printf("Name  :"); gets(add->std.name);
  printf("Grade :"); scanf("%d", &add->std.grade);
  add->next = NULL;
  if(root == NULL)//TH danh sach rong
    {
      root = add;
      return root;
    }
  if(add->std.grade >= root->std.grade)//Adding a address - beginning
    {
      add->next = root;
      root = add;
      return root;
    }
  curr_std = root;
  while(curr_std!=NULL && add->std.grade < curr_std->std.grade)//Adding a address - mid/end
    {
      prev_std = curr_std;
      curr_std = curr_std->next;
    }
  prev_std->next = add;
  add->next = curr_std;
  return root;
}

StudentList *remove_student()
{
  StudentList *curr_std, *prev_std;
  char SHSV[ID_LENGTH];
  printf("Ma so SV cua nguoi can xoa: ");
  gets(SHSV);
  if(root == NULL) return root;
  curr_std = root;
  if(strcmp(curr_std->std.ID, SHSV) == 0)//Removing a address - beginning
    {
      root = root->next;
      free(curr_std);
      curr_std = root;
    }
  while(curr_std!=NULL && strcmp(curr_std->std.ID, SHSV)!=0)//Removing a address - mid/end
    {
      prev_std = curr_std;
      curr_std = curr_std->next;
    }
  if(curr_std!=NULL)
    {
      prev_std->next = curr_std->next;
      free(curr_std);
    }
    else printf("Khong tim thay hoc sinh co ID = %s\n",SHSV);
  return root;
}

void enter()
{
  Student tmp;
  int i = 0;
  printf("Ket thuc viec nhap neu ID = 0\n");
  printf("Hay nhap hoc sinh theo thu diem giam dan:\n");
  while(1)
    {
      printf("Nguoi thu %d:\n",i++);
      printf("\tID    :"); gets(tmp.ID);
      if(strcmp(tmp.ID,"0") == 0) break;
      printf("\tName  :"); gets(tmp.name);
      printf("\tGrade :"); scanf("%d", &tmp.grade);
      while(getchar()!='\n');
      new_student(tmp);
    }
}

void print_list()
{
  int i = 0;
  StudentList *tmp;
  tmp = root;
  while(tmp!=NULL)
    {
      printf("Nguoi thu %d\n",i++);
      printf("\t %s %s %d\n", tmp->std.ID, tmp->std.name, tmp->std.grade);
      tmp = tmp->next;
    }
}

int main()
{
  StudentList *tmp;
  int choice;
  enter();
  do
    {
      printf("(1)Insert (2)Remove (3)Find (4)ChangeGrade (0)Exit\n");
      printf("Choice = "); scanf("%d", &choice);
      while(getchar()!='\n');
      switch(choice)
	{
	case 1: insert_student();
	  printf("\nAfter insert, student list: \n");
	  print_list();
	  break;
	case 2: remove_student();
	  printf("\nAfter remove, student list: \n");
	  print_list();
	  break;
	case 3: tmp = find_stduent();
	  if(tmp != NULL) printf("\tThong tin: %s %s %d\n", tmp->std.ID, tmp->std.name, tmp->std.grade);
	  else printf("Khong tim thay sinh vien trong sanh sach\n");
	  break;
	case 4: tmp = change_grade();
	  if(tmp != NULL)
	    {
	      printf("Thong tin sainh vien sau khi cap nhat:\n");
	      printf("\t%s %s %d\n", tmp->std.ID, tmp->std.name, tmp->std.grade);
	    }else
	    printf("Khong tim thay sinh vien trong sanh sach\n");
	}
    }while(choice!=0);
  printf("Number of student: %d\n", count_student());
  tmp = root;
  while(tmp!=NULL)
    {
      root = root->next;
      free(tmp);
      tmp = root;
    }
  return 0;
}







