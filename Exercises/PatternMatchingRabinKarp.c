/*Boyer - Rabin - Karp Matching Algorithm - C2: Cai tien cach tinh mang skip*/
#include <stdio.h>
#include <string.h>
#define MAX 256
void Rabin_Karp(char *source, char *pattern)
{
  int skip[MAX], i, j, m, n, num, modul=10, offset;
  n = strlen(source);
  m = strlen(pattern);
  num = 0;
  for(i=0; i<m; i++) num = 2*num + pattern[i];
  skip[0] = 0;
  offset = 1;
  for(i=0; i<m-1; i++) offset = 2*offset;//offset = 2^(m-1)
  for(i=0; i<m; i++) skip[0] = 2*skip[0] + source[i];
  for(i=1; i<=n-m; i++) skip[i] = 2*(skip[i-1] - offset*source[i-1]) + source[i+m-1];
  for(i=0; i<=n-m; i++)
  {
    if(num == skip[i]) printf("Chuoi tim thay tai chi so %d\n", i);
  }
}

int main()
{
  char source[MAX], pattern[MAX];
  printf("Nhap vao chuoi nguon : ");
  gets(source);
  printf("Nhap vao chuoi tim kiem : ");
  gets(pattern);
  Rabin_Karp(source, pattern);
  return 0;
}
