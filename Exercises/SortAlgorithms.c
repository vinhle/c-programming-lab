#include <stdio.h>

//Swap two elements of array a
//We assume that i and j are valid indexes of a
void swap(int *a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}

void print(int *a, int n) {
  if (n==0)
    printf("[]\n");
  else {
    printf("[%d", a[0]);
    for(int i=1; i<n; i++)
      printf(", %d", a[i]);
    printf("]\n");
  }
}

// Sorting function based on the quick sort algorithm
// Pivot is chosen as the last element of the array
void qsort(int *a, int n) {
  if (n <= 1) return;
  
  int p = a[n-1]; //Pivot is the last element
  int i = 0;
  for (int j=0; j<n-1; j++) {
    if (a[j] < p) {
      swap(a, i, j);
      i++;
    }
  }
  swap(a, i, n-1);

  qsort(a, i);
  qsort(a+i+1, n-i-1);
}  

// Sorting function based on the merge sort algorithm
void msort(int *a, int n) {
  if (n <= 1) return;
  
  int mid = n/2;
  
  msort(a, mid);
  msort(a+mid, n-mid);

  int tmp[n];
  for(int i=0; i<n; i++)
    tmp[i] = a[i];
  
  int p1 = 0;
  int p2 = mid;
  
  for(int i=0; i<n; i++)
    if( p1 < mid )
      if( p2 < n )
	if( tmp[p1] < tmp[p2] )
	  a[i] = tmp[p1++];
	else
	  a[i] = tmp[p2++];
      else
	a[i] = tmp[p1++];
    else
      a[i] = tmp[p2++];
}

int main() {
  int a[10] = {2,8,7,1,3,9,3,5,6,4};
  int b[10] = {2,8,7,1,3,9,3,5,6,4};
  print(a,10);
  qsort(a,10);
  print(a,10);
  msort(b,10);
  print(b,10);
}












