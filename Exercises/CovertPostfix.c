/* In bieu thuc dang hau to cua mot bieu thuc trung to */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
typedef char ele_type;
typedef struct node
{
  ele_type data;
  struct node *link;
}stack;

int Is_empty(stack *top)
{
  return (top->link == NULL);
}
void Push(stack **top, ele_type value)
{
  stack *temp;
  temp = (stack *)malloc(sizeof(stack));
  if (temp == NULL)
    {
      printf("No memory available Error\n");
      exit(0);
    }
  temp->data = value;
  temp->link = (*top);
  (*top) = temp;
}
ele_type Pop(stack **top)
{
  struct node *temp;
  ele_type value;
  if(top == NULL)
    {
      printf("The stack is empty can not Pop Error\n");
      exit(0);
    }
  value = (*top)->data;
  temp = (*top);
  (*top) =(*top)->link;
  free(temp);
  return value;
}

int Muc_uutien(char ch1,char ch2)
{
  if(ch1=='(') return 0;
  if(ch1=='+'||ch1=='-')
    {
      if(ch2=='+'||ch2=='-') return 0;//Dong muc uu tien
      if(ch2=='*'||ch2=='/') return -1;//ch1 co muc uu tien thap hon ch2
    }
  if(ch1=='*'||ch1=='/')
    {
      if(ch2=='+'||ch2=='-') return 1;//ch1 co muc uu tien cao hon ch2
      if(ch2=='*'||ch2=='/') return 0;//Dong muc uu tien
    }
}

void Postfix_Notation(char *str1, char *str2, stack **top)
{
    ele_type value;
    int i=0, j=0;
    while(i < strlen(str1))
    {
      if(str1[i]>='0' && str1[i]<='9')
      {
        while(str1[i]>='0' && str1[i]<='9')//Dung vong lap vi mot so co nhieu chu so
        {
          str2[j++] = str1[i];
          i++;
        }
        str2[j++] = ' ';
      }
      if(str1[i] == '(') Push(top, str1[i]);
      if(str1[i]=='+' || str1[i]=='-' || str1[i]=='*' || str1[i]=='/')
      {
        while(*top!=NULL && Muc_uutien((*top)->data,str1[i])>=0)
        {
            str2[j++] = Pop(top);
            str2[j++] = ' ';
        }
        Push(top, str1[i]);
      }
      if(str1[i] == ')')
      {
        while(1)
        {
          value = Pop(top);
          if(value == '(') break;
          str2[j++] = value;
          str2[j++] = ' ';
        }
      }
      i++;
    }
    while(*top != NULL)//De dua ra not cac dau con lai trong ngan xep
    {
        str2[j++] = Pop(top);
        str2[j++] = ' ';
    }
    str2[j] = '\0';
}

void Free_stack(stack **top)
{
  stack *tmp;
  tmp = *top;
  while (tmp != NULL)
    {
      *top = (*top)->link;
      free(tmp);
      tmp = *top;
    }
}

int main()
{
    stack *top = NULL;
    char str1[100], str2[100];
    printf("Input bieu thuc trung to: "); gets(str1);
    Postfix_Notation(str1, str2, &top);
    printf("Output bieu thuc hau to : %s\n", str2);
    Free_stack(&top);
    return 0;
}
