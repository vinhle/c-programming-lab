/*Boyer - Moore's Pattern Matching Algorithm - C2: Theo Cach cau T. Cao Tuan Dung*/
#include <stdio.h>
#include <string.h>
#define MAX 256 //con so can thiet de chua tat ca cac ki tu tren ban phim
int BoyerMoore(char *source, char *pattern)
{
  int skip[MAX], i = 0, m, j=-1, n;
  n = strlen(source);
  m = strlen(pattern);
  for(i=0; i<MAX; i++) skip[i] = m-1;//Initial skip function
  for(i=0; i<m; i++)
    if(skip[pattern[i]] == m-1) skip[pattern[i]] = m-i-1;//mang luu do bo qua cac chu khi duyet

  i = j = m-1;
  do
  {
    if(source[i] == pattern[j])
    {
      i--;
      j--;
    }
    else
    {
      if(m-j+1 > skip[source[i]]) i += m-skip[source[i]]+1;//j+1;
      else i += m;
      j = m-1;
    }
  } while (j>=0 && i<n);
  if(j <= 0) return i+1;
  else return -1;
}

int main()
{
  char source[MAX], pattern[MAX];
  int f;
  printf("\nNhap vao chuoi nguon : ");
  gets(source);
  printf("Nhap vao chuoi tim kiem : ");
  gets(pattern);
  f = BoyerMoore(source, pattern);
  printf("f = %d\n",f);

  if (f >= 0)
    printf("Chuoi tim thay tai chi so %d\n", f);
  else
    printf("Khong tim thay chuoi da cho\n", f);
  return 0;
}
