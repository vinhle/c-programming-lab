//Bubble Sort - Sap xep noi bot
#include<stdio.h>
#define MAX 100
typedef int ele_type;
//Noi bot phan tu max xuong cuoi
void Bubble_sort1(ele_type array[], int size)
{
  int i, j;
  ele_type tmp;
  for(i=(size-1); i>=0; i--)
    for(j=0; j<=i; j++)
    {
      if(array[j] > array[i])
      {
        tmp = array[j];
        array[j] = array[i];
        array[i] = tmp;
      }
    }
}
//Noi bot phan tu min len dau
void Bubble_sort2(ele_type array[], int size)
{
  int i, j;
  ele_type tmp;
  for(i=0; i<size-1; i++)
    for(j=i+1; j<size; j++)
    {
      if(array[j] < array[i])
      {
        tmp = array[j];
        array[j] = array[i];
        array[i] = tmp;
      }
    }
}

int main()
{
  ele_type array[MAX];
  int size,i;
  printf("Number of list is:  ");
  scanf("%d", &size);
  printf("Number in list are: ");
  for(i=0; i<size; i++)  scanf("%d",&array[i]);

  printf("After sorted by Insertion_sort: ");
  Bubble_sort2(array, size);
  for(i=0; i<size; i++)  printf("%d ",array[i]);
  printf("\n");
  return 0;
}
