//Insertion Sort
#include<stdio.h>
#define MAX 100
typedef int ele_type;

void Insertion_sort(ele_type array[], int size)
{
  int i, j;
  ele_type next;
  for (i=1; i<size; i++)
  {
    next = array[i];
    j = i;
    while(j>0 && next<array[j-1])
    {
      array[j] = array[j-1];
      j--;
    }
    array[j] = next;
  }
}

int main()
{
  ele_type array[MAX];
  int size,i;
  printf("Number of list is:  ");
  scanf("%d", &size);
  printf("Number in list are: ");
  for(i=0; i<size; i++)  scanf("%d",&array[i]);

  printf("After sorted by Insertion_sort: ");
  Insertion_sort(array, size);
  for(i=0; i<size; i++)  printf("%d ",array[i]);
  return 0;
}
