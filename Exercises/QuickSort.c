//Quick Sort
#include<stdio.h>
#define MAX 100
typedef int ele_type;

int Partition(ele_type array[], int L, int R)
{
  int pivot; //Gia tri chot
  int i, j;
  ele_type temp;
  i = L; j = R+1;
  pivot = array[L]; //Chon nut dau lam nut truoc
  do
  {
    do i++; while ((array[i]<pivot) && (i<=R));
    do j--; while ((array[j]>pivot) && (j>=L));
    if(i < j)//doi cho cac node tai i va j
    {
      temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }while (i < j);
  // doi cho hai nut tai L va j de cho chot vao giua
  array[L] = array[j];
  array[j] = pivot;
  return j;
}

void Quick_sort(ele_type array[], int left, int right)
{
  int pivot;//Chi so vi tri cua chot
  if(left >= right) return; //Dieu kien dung
  if(left < right)
  {
    pivot = Partition(array, left, right);
    Quick_sort(array, left, pivot-1);
    Quick_sort(array, pivot+1, right);
  }
}

int main()
{
  ele_type array[MAX];
  int i, size;
  printf("Number of array is:  ");
  scanf("%d", &size);
  printf("Number in array are: ");
  for(i=0; i<size; i++)  scanf("%d",&array[i]);

  printf("After sorted by Quick_sort: ");
  Quick_sort(array, 0, size-1);
  for(i=0; i<size; i++)  printf("%d ",array[i]);
  printf("\n");
  return 0;
}
