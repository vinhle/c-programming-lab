#include<stdio.h>

// Compute the greatest common divisor
int gcd1(int m, int n)
{
    while(n != 0)
    {
        int temp = m;
        m = n;
        n = temp % n;
    }
    return m;
}

int gcd2(int m, int n)
{
    if(n == 0) return m;
    else return gcd2(n, m%n);
    //return (n==0) ? m : gcd2(n, m%n);
}

int main()
{
    int m, n;
    printf("Compute the greatest common divisor \n");
    printf("First number: "); scanf("%d", &m);
    printf("Second number: "); scanf("%d", &n);

    printf("GCD1 is: %d \n", gcd1(m, n));
    printf("GCD2 is: %d \n", gcd2(m, n));

    return 0;
}
