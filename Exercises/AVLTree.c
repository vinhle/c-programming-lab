#include<stdio.h>
#include<stdlib.h>

struct nodeAVL {
    int x;
    int height;
    struct nodeAVL* left;
    struct nodeAVL* right;
    struct nodeAVL* parent;
};
typedef struct nodeAVL* avl;

// Constructor
avl nodeAVL(int x)
{
    avl tmp = malloc(sizeof(struct nodeAVL));
    tmp->x = x;
    tmp->height = 0;
    tmp->left   = NULL;
    tmp->right  = NULL;
    tmp->parent = NULL;
}

void printAVL(avl t)
{
    if(t == NULL) printf("L");
    else
    {
        printf("N(");
        printAVL(t->left);
        printf(", (%d-%d), ", t->x,t->height);
        printAVL(t->right);
        printf(")");
    }
}

// Compute the height of tree
void updateHeight(avl a)
{
    if(a->left != NULL)
        if(a->right != NULL)
            if(a->left->height > a->right->height)
                a->height = a->left->height + 1;
            else
                a->height = a->right->height + 1;
        else
            a->height = a->left->height + 1;
    else
        if(a->right != NULL)
            a->height = a->right->height + 1;
        else
            a->height = 0;
}

// Rotation right
void rotationR(avl *p)
{
    avl a    = (*p);
    avl l    = (*p)->left;
    a->left  = l->right;
    updateHeight(a);
    l->right = a;
    updateHeight(l);
    l->parent = a->parent;
    a->parent = l;
    if(a->left != NULL) a->left->parent = a;
    (*p) = l;
}
// Rotation left
void rotationL(avl *p)
{
    avl a    = (*p);
    avl r    = (*p)->right;
    a->right = r->left;
    updateHeight(a);
    r->left = a;
    updateHeight(r);
    r->parent = a->parent;
    a->parent = r;
    if(a->right != NULL) a->right->parent = a;
    (*p) = r;
}

// Compute the balance of tree
int balance(avl a) {
    return
        ((a->left != NULL) ? a->left->height : -1) -
        ((a->right != NULL) ? a->right->height : -1);
}
void updateBalance(avl *p)
{
    avl a = *p;
    int bal = balance(a);
    if(bal > 1)
    {
        if(balance(a->left) < 0) rotationL(&(a->left));
        rotationR(p);
    }
    else if(bal < -1)
        {
            if(balance(a->right) > 0) rotationR(&(a->right));
            rotationL(p);
        }
}

void addAVL(avl *p, int x)
{
    avl a = *p;
    if(a == NULL) *p = nodeAVL(x);
    else {
        // Tree is not empty
        // Find where to add x
        while((x < a->x && a->left != NULL) || (x > a->x && a->right != NULL))
        {
            if(x < a->x) a = a->left;
            else a = a->right;
        } // Found location to add x

        // Nothing to do if x already exists
        if(x == a->x) return;

        if(x < a->x) {
            a->left = nodeAVL(x);
            a->left->parent = a;
        }
        if(x > a->x) {
            a->right = nodeAVL(x);
            a->right->parent = a;
        }

        // Addition finished
        // Need to update height and balance from adding position up to the root
        while(a->parent != NULL) {
            avl *temp;
            if(a->parent->left == a) temp = &(a->parent->left);
            else temp = &(a->parent->right);
            updateHeight(a);
            updateBalance(&a);
            (*temp) = a;
            a = a->parent;
        }
        updateHeight(a);
        updateBalance(&a);
        *p = a;
    }
}

int main()
{
    avl t = NULL;
    addAVL(&t,7);
    addAVL(&t,49);
    addAVL(&t,73);
    addAVL(&t,58);
    addAVL(&t,30);
    addAVL(&t,72);
    addAVL(&t,44);
    addAVL(&t,78);
    addAVL(&t,23);
    addAVL(&t,9);
    printAVL(t);
    printf("\n");
}
