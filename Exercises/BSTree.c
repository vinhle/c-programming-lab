#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h>

// Construction
struct node
{
    int x;
    struct node *left, *right;
};
typedef struct node* bstree;

// Constructor
bstree node(bstree l, int x, bstree r)
{
    bstree t = malloc(sizeof(struct node));
    t->x     = x;
    t->left  = l;
    t->right = r;
}

// Destructor
void free_tree(bstree t)
{
    if(t != NULL)
    {
        free_tree(t->left);
        free_tree(t->right);
        free(t);
    }
}

// The printing function
void print_tree(bstree t)
{
    if(t == NULL) printf("L");
    else
    {
        printf("N(");
        print_tree(t->left);
        printf(", %d, ", t->x);
        print_tree(t->right);
        printf(")");
    }
}

// Membership test
bool member(int x, bstree t)
{
    if(t == NULL) return false;
    else
    {
        if(x == t->x) return true;
        if(x > t->x) return member(x, t->right);
        else return member(x, t->left);
    }
    // return (t!=NULL && ((x==t->x) || (x<t->x && member(x,t->left)) || (x>t->x) && member(x,t->right)));
}

// Add elemtent, only create new node in compare path
bstree addPer(int x, bstree t)
{
    bstree newtree;
    if(t == NULL) return node(NULL, x, NULL);
    if (x > t->x) return node(t->left, t->x, addPer(x,t->right));
    if (x < t->x) return node(addPer(x,t->left), t->x, t->right);
    else return t; // x == t->x
}

// Delete element
// Find the largest element in BSTree, return a pointer to this element
bstree find_largest(bstree t)
{
    if(t == NULL) return NULL;
    //if(t->right != NULL) return find_largest(t->right);
    //else return t;
    while(t->right != NULL) t = t->right;
    return t;
}
bstree remove_largest(bstree t)
{
    if(t == NULL) return NULL;
    if(t->right != NULL) return node(t->left, t->x, remove_largest(t->right));
    else return t->left;
}

bstree delPer(int x, bstree t)
{
    if(t == NULL) return NULL;
    if(x <  t->x) return node(delPer(x,t->left), t->x, t->right);
    if(x >  t->x) return node(t->left, t->x, delPer(x,t->right));
    if(x == t->x)
    {
        // Node to be removed has no children or one child
        if(t->left  == NULL) return t->right;
        if(t->right == NULL) return t->left;
        // Swap value of largest node in left child with current node
        // Node that now t is pointing to current node
        bstree largest = find_largest(t->left);
        return node(remove_largest(t->left), largest->x, t->right);
    }
}

// Main function
int main()
{
    bstree t = node(node(NULL,2,NULL),3,node(node(node(NULL,6,NULL),8,node(NULL,9,NULL)),10,node(NULL,12,NULL)));
    t = addPer(5,t);
    t = addPer(7,t);
    print_tree(t); printf("\n");

    int a;
    // Add 9 is a good example
    printf("Please input your element to add: "); scanf("%d", &a);
    bstree t2 = addPer(a,t);
    print_tree(t2); printf("\n");

    int b;
    // Add 8 is a good example
    printf("Please input your element to delete: "); scanf("%d", &b);
    bstree t3 = delPer(b,t);
    printf("Delete in persistent style: \n");
    print_tree(t3); printf("\n");

    int x = 2, y = 8;
    printf("%d is a member of the tree -> %s \n", x, member(x,t) ? "true" : "false");
    printf("%d is a member of the tree -> %s \n", y, member(y,t) ? "true" : "false");

    //print_tree(t);
    free_tree(t);
    return 0;
}

