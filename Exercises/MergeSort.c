//Merge Sort
#include<stdio.h>
#define MAX 100
typedef int ele_type;
//Chu y trong ham phu khong khai bao so ph/t ma so ph/t la last theo de quy
void Merge(ele_type array[],int first, int mid, int last)
{
	ele_type temp[MAX];//mang phu
	int first1 = first;  int last1 = mid;
	int first2 = mid+1;  int last2 = last;
  int index = first1;
	while(first1<=last1 && first2 <= last2)//So sanh cac phan tu va gan vao mang phu
	{
		if(array[first1] < array[first2])
		{
			temp[index++] = array[first1];
			first1++;
		}
		else
		{
			temp[index++] = array[first2];
			first2++;
		}
	}
  for(; first1<=last1; first1++)//Sao not day con 1
    temp[index++] = array[first1];
  for(; first2<=last2; first2++)//Sao not day con 2
    temp[index++] = array[first2];
  for(index=first; index<=last; index++)//Sao tra mang ket qua
		array[index] = temp[index];
}
void Merge_sort(ele_type array[], int first, int last)
{
	int mid;
	if(first < last)
	{
    mid =(first+last)/2;
    Merge_sort(array, first, mid);
    Merge_sort(array, mid+1, last);
    Merge(array, first, mid, last);
	}
}

int main()
{
  ele_type array[MAX];
  int size,i;
  printf("Number of list is:  ");
  scanf("%d", &size);
  printf("Number in list are: ");
  for(i=0; i<size; i++)  scanf("%d",&array[i]);

  printf("After sorted by Merge_sort: ");
  Merge_sort(array, 0, size-1);
  for(i=0; i<size; i++)  printf("%d ",array[i]);
  printf("\n");
  return 0;
}
