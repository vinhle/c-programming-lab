//Kiem tra dau ngoac dung
#include <stdio.h>
#include <string.h>
#define MAX 100
typedef char ele_type;
typedef ele_type StackType[MAX];
int top;

void Initialize(StackType stack)
{
  top = -1;
}
int Is_empty(StackType stack)
{
  if(top == -1) return 1;
  else return 0;
}
int Is_full(StackType stack)
{
  if(top == MAX-1) return MAX;
  else return 0;
}
void Push(ele_type n, StackType stack)
{
  if(Is_full(stack)) printf("stack overflow\n");
  else stack[++top] = n;
}
ele_type Pop(StackType stack)
{
  if(top < 0)
  {
    printf("\nstack underflow\n");
    return 0;
  }
  return stack[top--];
}

void Dau_ngoac(StackType stack, char *str)
{
	int i;
	char ch;
	for(i=0; i<strlen(str); i++)
	{
		if (str[i]=='(' || str[i]=='[' || str[i]=='{' )  Push(str[i], stack);
		if (str[i]==')'|| str[i]==']' || str[i]=='}')
		{
		  if(top == -1) {printf("Sai\n"); return;}//Truong hop het stack ma van con ) ] }
		  ch = Pop(stack);
		  if(ch != '(' && str[i] == ')') break;
		  if(ch != '[' && str[i] == ']') break;
		  if(ch != '{' && str[i] == '}') break;
		}
	}
	if (top == -1) printf("\nDung\n");
	else printf("\nSai\n");
}
int main()
{
  int i;
  char str[MAX];
  StackType stack;
  Initialize(stack);
  printf("Nhap vao 1 chuoi: "); gets(str);
  printf("Day co phai dau ngoac dung khong: ");
  Dau_ngoac(stack, str);
  return 0;
}
