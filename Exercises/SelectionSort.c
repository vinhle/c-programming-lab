//Insertion Sort
#include<stdio.h>
#define MAX 100
typedef int ele_type;

void Selection_sort(ele_type array[], int size)
{
  int i, j, min;
  ele_type tmp;
  for (i=0; i<size-1; i++)
  {
    min = i;
    for (j=i+1; j<size; j++)
      if (array[j] < array[min]) min = j;
    tmp = array[i];
    array[i]= array[min];
    array[min] = tmp;
  }
}

int main()
{
  ele_type array[MAX];
  int size,i;
  printf("Number of list is:  ");
  scanf("%d", &size);
  printf("Number in list are: ");
  for(i=0; i<size; i++)  scanf("%d",&array[i]);

  printf("After sorted by Selection_sort: ");
  Selection_sort(array, size);
  for(i=0; i<size; i++)  printf("%d ",array[i]);
  return 0;
}
