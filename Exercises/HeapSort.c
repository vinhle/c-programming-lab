//Heap Max Sort - Chu y mang day duoc bat dau tu 1
#include<stdio.h>
#define MAX 100
typedef int ele_type;

void Adjust(ele_type addr[], int size, int root)//Vun dong
{
  int child, rootkey;
  ele_type temp;
  temp = addr[root];
  rootkey = addr[root];
  child = 2*root;
  while(child <= size)
  {
    if(child < size && addr[child] < addr[child+1]) child++;
    if(rootkey > addr[child]) break;
    else
    {
      addr[child/2] = addr[child];
      child = child * 2;
    }
  }
  addr[child/2] = temp;
}
void Heap_max_sort(ele_type addr[], int size)
{
  int i, j;
  ele_type temp;
  for (i = size/2; i>0; i--) Adjust(addr, size, i);//Xay dung dong
  for (i = size-1; i>0; i--)
  {
    temp = addr[1];
    addr[1] = addr[i+1];
    addr[i+1] = temp;
    Adjust(addr, i, 1);//Vun lai dong
  }
}

int main()
{
  ele_type array[MAX];
  int size,i;
  printf("Number of list is:  ");
  scanf("%d", &size);
  printf("Number in list are: ");
  for(i=1; i<=size; i++)  scanf("%d",&array[i]);

  printf("After sorted by Heap_max_sort: ");
  Heap_max_sort(array, size);
  for(i=1; i<=size; i++)  printf("%d ",array[i]);
  return 0;
}
