#include <stdbool.h>
#include <stdio.h>

//Ugly (but simple) way of defining infinity
#define INFTY 10000

int findMin(int *m, bool *visited, int n) {
  int minValue = INFTY;
  int minIndex = -1;
  for(int i=0; i<n; i++)
    if (!visited[i] && m[i] < minValue) {
      minValue = m[i];
      minIndex = i;
    }
  return minIndex;
}


void dijkstra(int **g, int n) {
  int m[n];
  for(int i=0; i<n; i++)
    m[i]=INFTY;
  
  bool visited[n];
  m[0] = 0;

  int next = 0;
  do {
    visited[next] = true;
    for(int j = 0; j < n; j++)
      if (g[next][j] != INFTY)
	if (m[next] + g[next][j] < m[j])
	  m[j] = m[next] + g[next][j];
    next = findMin(m, visited, n);
  } while (next!=-1);

  for(int i=0; i<n; i++)
    printf("From 0 to %d, the minimum path has length %d \n", i, m[i]);
}


int main() {
  //Defining a graph a with a adjacency matrix
  //Be careful with the ugly "int *a[5]" => a is an array of pointers of size 5.
  int a0[5] = {INFTY, 9, INFTY, 3, INFTY};
  int a1[5] = {9, INFTY, INFTY, INFTY, 2};
  int a2[5] = {INFTY, 1, INFTY, INFTY, INFTY};
  int a3[5] = {INFTY, 4, INFTY, INFTY, 8};
  int a4[5] = {INFTY, INFTY, 1, INFTY, INFTY};
  int *a[5] = {a0, a1, a2, a3, a4};

  dijkstra(a, 5); 

  return 0;
}
	    
